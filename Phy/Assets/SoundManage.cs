﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManage : MonoBehaviour
{
    public GameObject Music1;
    public GameObject Music2;
    public GameObject Music3;
    public GameObject Music4;
    public GameObject Music5;
    public GameObject Music6;
    public GameObject Music7;
    public GameObject Music8;
    public GameObject Music9;
    public GameObject Music10;
    public GameObject Music11;
    public GameObject Music12;
    public GameObject Music13;
    public AudioSource AudioSource1;
    public AudioSource AudioSource2;
    public AudioSource AudioSource3;
    public AudioSource AudioSource4;
    public AudioSource AudioSource5;
    public AudioSource AudioSource6;
    public AudioSource AudioSource7;
    public AudioSource AudioSource8;
    public AudioSource AudioSource9;
    public AudioSource AudioSource10;
    public AudioSource AudioSource11;
    public AudioSource AudioSource12;
    public AudioSource AudioSource13;
    private int sound;
    void Start()
    {
        sound = 1;
        AudioSource1.volume = 0f;
        AudioSource2.volume = 0f;
        AudioSource3.volume = 0f;
        AudioSource4.volume = 0f;
        AudioSource5.volume = 0f;
        AudioSource6.volume = 0f;
        AudioSource7.volume = 0f;
        AudioSource8.volume = 0f;
        AudioSource9.volume = 0f;
        AudioSource10.volume = 0f;
        AudioSource11.volume = 0f;
        AudioSource12.volume = 0f;
        AudioSource13.volume = 0f;
    }
    void Update()
    {
            if(Music3 == null)
            {
                AudioSource13.volume = 1f;
            }
            if(Music12 == null)
            {
                AudioSource12.volume = 1f;
            }
            if(Music11 == null)
            {
                AudioSource11.volume = 1f;
            }
            if(Music10 == null)
            {
                AudioSource10.volume = 1f;
            }
            if(Music9 == null)
            {
                AudioSource9.volume = 1f;
            }
            if(Music8 == null)
            {
                AudioSource8.volume = 1f;
            }
            if(Music7 == null)
            {
                AudioSource7.volume = 1f;
            }
             if(Music6 == null)
            {
                AudioSource6.volume = 1f;
            }
                 if(Music5 == null)
            {
                AudioSource5.volume = 1f;
            }
                 if(Music4 == null)
            {
                AudioSource4.volume = 1f;
            }
                 if(Music3 == null)
            {
                AudioSource3.volume = 1f;
            }
                 if(Music2 == null)
            {
                AudioSource2.volume = 1f;
            }
                 if(Music1 == null)
            {
                AudioSource1.volume = 1f;
            }
        Debug.Log(sound);
    }
}
