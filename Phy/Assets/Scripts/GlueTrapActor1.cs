﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlueTrapActor1 : MonoBehaviour {
    FixedJoint _fixedJoint = null;
    public Rigidbody rb;
    public float timeRemaining = 5;
    public bool timerIsRunning = false;
    public AudioSource Wall;

    // Start is called before the first frame update
    void Start () {
        rb = GetComponent<Rigidbody> ();
    }

    // Update is called once per frame
    void Update () {
        if (timerIsRunning) {
            if (timeRemaining > 0) {
                timeRemaining -= Time.deltaTime;
            } else {
                Debug.Log ("Time has run out!");
                timeRemaining = 0;
                timerIsRunning = false;
            }
        }
    }
    void OnCollisionEnter (Collision collision) {
        Wall.Play(0);
        GameObject go = collision.gameObject;
        GlueTrap1 gluetrap = go.GetComponent<GlueTrap1> ();
        if (gluetrap != null) {
            _fixedJoint = this.gameObject.AddComponent<FixedJoint> ();
            _fixedJoint.connectedBody = collision.gameObject.GetComponent<Rigidbody> ();
            Invoke ("ReleaseFixedJoint", gluetrap.TimeToSetFree);
        }
    }
    void OnCollisionStay (Collision other) {
            timerIsRunning = true;
            rb.drag = timeRemaining;
    }
    void OnCollisionExit (Collision other) {
            timeRemaining = 5;
            rb.drag = 0;
    }
    void ReleaseFixedJoint () {
        _fixedJoint.connectedBody = null;
        Destroy (_fixedJoint);
    }
}