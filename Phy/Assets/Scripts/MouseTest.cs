﻿using UnityEngine;

public class MouseTest : MonoBehaviour {
    [HideInInspector]
    public Rigidbody rb;
    [HideInInspector]
    public Vector3 mMouseDownPos;
    [HideInInspector]
    public Vector3 mMouseUpPos;
    [HideInInspector]
    public Vector3 MaxMouseY;
    [HideInInspector]
    public Vector3 MaxMouseX;

    public GameObject DeathEffect;
    public Transform Player;
    public GameObject Retry;

    void start () {
        rb = GetComponent<Rigidbody> ();
        MaxMouseX.x = 80;
        MaxMouseX.y = 80;
        MaxMouseY.x = -80;
        MaxMouseY.y = -80;
    }
    void Update()
    {
        Vector3 temp = Input.mousePosition;
        temp.z = 53f; // Set this to be the distance you want the object to be placed in front of the camera.
    }
    void OnMouseDown () {
        mMouseDownPos = Input.mousePosition;
        mMouseDownPos.z = 0;
    }

    void OnMouseUp () {
        mMouseUpPos = Input.mousePosition;
        mMouseUpPos.z = 0;
        Vector3 direction = mMouseDownPos - mMouseUpPos;
        direction.Normalize();
        rb.AddForce (direction*200f,ForceMode.Impulse);
        Debug.Log(direction);
    }
    void OnTriggerEnter(Collider other) {
            if (other.gameObject.tag == "Orbs") {
            Instantiate(DeathEffect,Player);
            Retry.SetActive(true);
            Destroy (this.gameObject);
    }
    }
}