﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraUp : MonoBehaviour
{
    public Transform PlayerTransform;
    public Transform CameraTransform;
    public float Velocity;
    public float Speed;
    public Transform target;

    void Start()
    {
        if(WallSpawner.StartGame == true)
        {
        Velocity = 1f;
        Speed = 1f;
        StartCoroutine(Count());
        }
    }
    void Update()
    {
        if(WallSpawner.StartGame == true)
        {
        transform.Translate(Vector3.up * Time.deltaTime *Speed, Space.World);
        if (PlayerTransform.position.y > CameraTransform.position.y)
        {
            Debug.Log("Up");
            CameraTransform.Translate(Vector3.up * (PlayerTransform.position.y - CameraTransform.position.y), Space.World);
        }
        }
    }
    IEnumerator Count()
    {
        while(true)
        {
            Velocity = Velocity+Time.deltaTime;
            Speed = Speed+Velocity;
            yield return new WaitForSeconds(10f);
        }
        
    }
}
