﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerColor : MonoBehaviour
{
    [SerializeField] private Material material;
    void Update()
    {
        material.color = Random.ColorHSV(0f, 1f, 1f, 1f, 0.8f, 0.8f);
    }
}
