﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    public GameObject three;
    public GameObject Game;
    public Button yourButton;
    Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        Button btn = yourButton.GetComponent<Button>();
		btn.onClick.AddListener(TaskOnClick);
    }

    // Update is called once per frame
        void TaskOnClick(){
        StartCoroutine(ClickStart());

	}
    IEnumerator ClickStart()
    {
        yield return new WaitForSeconds(1.5f);
        Game.SetActive(false);
        anim.SetTrigger("Count");
        Destroy(three,2.5f);
    }
}
