﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColor : MonoBehaviour
{
    [SerializeField] private Material material;
    void OnCollisionEnter(Collision collision)
    {
        material.color = Random.ColorHSV(0f, 1f, 1f, 1f, 0.8f, 0.8f);
    }
}
