﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManeger : MonoBehaviour
{
    public static float distance;
    public Transform player;
    public Transform StartPoint;
    public Text scoreVal;
    public static int scores;

    void Start()
    {
        transform.position=player.position;
        distance=0;
    }

    void Update()
    {
    distance = (player.transform.position.y - StartPoint.transform.position.y);
    distance = distance + scores;
    scoreVal.text = distance.ToString("0");
    
    }
    
}
