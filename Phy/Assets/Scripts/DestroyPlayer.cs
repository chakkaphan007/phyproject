﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyPlayer : MonoBehaviour
{
public GameObject Retry;
void OnTriggerEnter(Collider other)
{
    if(other.tag == "OutWall")
    {
        Destroy(this.gameObject);
        Retry.SetActive(true);
    }
}
}
